package custom;

public class Vector2D {
    public static Vector2D NULL_VECTOR = new Vector2D(0, 0);
    public static Vector2D FULL_NEGATIVE = new Vector2D(-1, -1);
    public static Vector2D FIRST_NEGATIVE = new Vector2D(-1, 1);
    public static Vector2D SECOND_NEGATIVE = new Vector2D(1, -1);
    public static Vector2D FIRST = new Vector2D(1, 0);
    public static Vector2D SECOND = new Vector2D(0, 1);

    private double x;
    private double y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void modifyAdd(Vector2D another) {
        x += another.x;
        y += another.y;
    }

    public Vector2D add(Vector2D another) {
        return new Vector2D(x + another.x, y + another.y);
    }

    public void modifySubtract(Vector2D another) {
        x -= another.x;
        y -= another.y;
    }

    public Vector2D subtract(Vector2D another) {
        return new Vector2D(x - another.x, y - another.y);
    }

    public void modifyMultiply(Vector2D another) {
        x *= another.x;
        y *= another.y;
    }

    public Vector2D multiply(Vector2D another) {
        return new Vector2D(x * another.x, y * another.y);
    }

    public void modifyDivide(Vector2D another) {
        x /= another.x;
        y /= another.y;
    }

    public Vector2D divide(Vector2D another) {
        return new Vector2D(x / another.x, y / another.y);
    }

    public Vector2D unitVector() {
        double norm = norm();
        return new Vector2D(x / norm, y / norm);
    }

    public double norm() {
        return Math.sqrt(x * x + y * y);
    }

    public void scaled(double scaleBy) {
        x *= scaleBy;
        y *= scaleBy;
    }

    public void toUnitVector() {
        double norm = norm();
        x /= norm;
        y /= norm;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

}

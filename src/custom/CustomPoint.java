package custom;

import java.util.Objects;

public class CustomPoint implements Comparable<CustomPoint> {
    private double x;
    private double y;
    private String name = "";
    private PointType type = PointType.NORMAL;

    public CustomPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static CustomPoint fromString(String content) {
        try {
            String[] splitted = Objects.requireNonNull(content)
                    .replace("(", "")
                    .replace(")", "")
                    .split(",");
            double x = Double.parseDouble(splitted[0].trim());
            double y = Double.parseDouble(splitted[1].trim());
            return new CustomPoint(x, y);
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot parse: '" + content + "'");
        }
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PointType getType() {
        return type;
    }

    public void setType(PointType type) {
        this.type = type;
    }

    @Override
    public int compareTo(CustomPoint o) {
        return o.name.compareTo(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomPoint point = (CustomPoint) o;

        return  Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return name;
    }

}

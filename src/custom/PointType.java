package custom;

public enum PointType {
    NORMAL,
    NEIGHBOUR,
    VISITED
}

package elektrotehnika;

import custom.Vector2D;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ChargeCollection {

    private List<Charge> chargeList = new ArrayList<>();

    public void forEachCharge(Consumer<Charge> action) {
        chargeList.forEach(action);
    }

    public Charge get(int index) {
        return chargeList.get(index);
    }

    public int numberOfCharges() {
        return chargeList.size();
    }

    public Charge closestTo(double x, double y) {
        double currentDistance = 10;
        Charge selected = null;

        for (Charge c : chargeList) {
            double x0 = c.getPosition().getX();
            double y0 = c.getPosition().getY();
            double distance = Math.sqrt((x0 - x) * (x0 - x) + (y0 - y) * (y0 - y));
            if (distance < currentDistance) {
                selected = c;
                currentDistance = distance;
            }
        }

        return selected;
    }

    public void add(Charge c) {
        if (!chargeList.contains(c)) {
            chargeList.add(c);
        } else {

        }
    }

    public Charge get(String name) {
        for (Charge c : chargeList) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public void remove(String identifier) {
        for (int i = 0; i < chargeList.size(); ++i) {
            Charge current = chargeList.get(i);
            if (current.getName().equals(identifier)) {
                chargeList.forEach(c -> c.unregisterForceFrom(current.getName()));
                chargeList.remove(i);
                return;
            }
        }
    }

    public boolean containsChargeAt(Vector2D center) {
        for (int i = 0; i < chargeList.size(); ++i) {
            Charge c = chargeList.get(i);
            if (Math.abs(c.getPosition().getX() - center.getX()) < 5 &&
                    Math.abs(c.getPosition().getY() - center.getY()) < 5) {
                return true;
            }
        }
        return false;
    }

    public void recalculateForces() {
        for (int i = 0; i < chargeList.size(); ++i) {
            for (int j = 0; j < chargeList.size(); ++j) {
                if (i != j) {
                    chargeList.get(i).registerForceFrom(chargeList.get(j));
                }
            }
        }
        chargeList.forEach(Charge::calculateTotalForce);
    }

    public void onChargeRenamed(String oldName, String newName) {
        forEachCharge(c -> {
            Vector2D oldForce = c.getForces().get(oldName);
            if (oldForce != null) {
                c.getForces().remove(oldName);
                c.getForces().put(newName, oldForce);
                c.calculateTotalForce();
            }
        });
    }

}

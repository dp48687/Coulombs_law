package elektrotehnika;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Launcher extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox pane = FXMLLoader.load(Launcher.class.getResource("coulombov_zakon.fxml"));
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("COULOMBOV ZAKON - TUTORIAL");
        primaryStage.show();
        primaryStage.setResizable(false);
    }

}

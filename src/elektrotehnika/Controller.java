package elektrotehnika;

import custom.Vector2D;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.Painter;
import utils.RandomUtils;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private Charge selectedCharge, toHighlight, toRefactor;
    private int vectorScaleConstant = 1;
    private ChargeCollection charges = new ChargeCollection();
    private static DecimalFormat formatter = new DecimalFormat("0.000##");

    @FXML
    private Slider scaleVectors;

    @FXML
    private Canvas canvas;

    @FXML
    private RadioButton uC, nC, pC, cm, mm, um, t1, t10, t100;

    @FXML
    private ToggleButton showVectors, showResultVector, changeCharge, deleteCharge;

    @FXML
    private Button add;

    @FXML
    private TextField label, chargeAmount;

    @FXML
    private Label trenutnaPozicija, absoluteForce;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        um.setText("\u03bcm");

        if (cm.isSelected()) {
            Info.distanceMeasure = 'c';
        } else if (mm.isSelected()) {
            Info.distanceMeasure = 'm';
        } else {
            Info.distanceMeasure = 'u';
        }

        showResultVector.setOnMouseClicked(
                event -> repaintAll()
        );
        showVectors.setOnMouseClicked(
                event -> repaintAll()
        );

        changeCharge.setOnMouseClicked(event -> {
            deleteCharge.setSelected(false);
            if (!changeCharge.isSelected() && toRefactor != null) {
                toRefactor.setType(ChargeType.NORMAL);
                Painter.drawCharge(canvas, toRefactor, 16);
                toRefactor = null;
            }
        });
        deleteCharge.setOnMouseClicked(event -> changeCharge.setSelected(false));

        cm.setOnMouseClicked(event -> {Info.distanceMeasure = 'c'; charges.recalculateForces();});
        mm.setOnMouseClicked(event -> {Info.distanceMeasure = 'm'; charges.recalculateForces();});
        um.setOnMouseClicked(event -> {Info.distanceMeasure = 'u'; charges.recalculateForces();});

        repaintBackground();

        canvas.setOnMousePressed(event -> {
            selectedCharge = selectedCharge != null ?
                    null :
                    charges.closestTo(event.getX(), event.getY());
            if (changeCharge.isSelected()) {
                Charge currentClosest = charges.closestTo(event.getX(), event.getY());
                if (currentClosest != null) {
                    toRefactor = currentClosest;
                }
            } else if (deleteCharge.isSelected()) {
                Charge willBeRemoved = charges.closestTo(event.getX(), event.getY());
                if (willBeRemoved != null) {
                    charges.remove(willBeRemoved.getName());
                }
                return;
            }
            if (selectedCharge != null) {
                absoluteForce.setText(
                        selectedCharge + "\n|F| = " + formatter.format(selectedCharge.getTotalForce().norm() / 1E2) + " mN"
                );
            }
            charges.forEachCharge(c -> c.setType(ChargeType.NORMAL));
            if (toHighlight != null) {
                toHighlight.setType(ChargeType.HIGHLIGHTED);
            }
            if (toRefactor != null) {
                toRefactor.setType(ChargeType.TO_REFACTOR);
            }
            repaintAll();
        });
        canvas.setOnMouseMoved(event -> {
            trenutnaPozicija.setText(
                            "TRENUTNA POZICIJA: \n" +
                            "(" + event.getX() + ", " + event.getY() + ")"
            );
            setChargeTypes();
            if (selectedCharge != null) {
                if (toHighlight != null) {
                    toHighlight.setType(ChargeType.NORMAL);
                }
                if (selectedCharge != null) {
                    absoluteForce.setText(
                            selectedCharge + "\n|F| = " + formatter.format(selectedCharge.getTotalForce().norm() / 1E2) + " mN"
                    );
                }
                selectedCharge.setType(ChargeType.HIGHLIGHTED);
                toHighlight = null;
                charges.recalculateForces();
                selectedCharge.setCenterX(event.getX());
                selectedCharge.setCenterY(event.getY());
                repaintAll();
            } else {
                charges.recalculateForces();
                repaintAll();
                toHighlight = charges.closestTo(
                        event.getX(), event.getY()
                );
                if (toHighlight != null) {
                    toHighlight.setType(ChargeType.HIGHLIGHTED);
                    Painter.drawCharge(canvas, toHighlight, 18);
                }
            }
            repaintAll();
        });

        scaleVectors.valueProperty().addListener((observable, oldValue, newValue) -> {
            selectScaleConstant();
            Info.vectorScaleConstant = newValue.doubleValue() * vectorScaleConstant;
            repaintAll();
        });

    }

    @FXML
    public void addCharge() {
        boolean repaint = false;

        try {
            String identifier = label.getText();

            double microcoulombs;
            if (uC.isSelected()) {
                microcoulombs = Double.parseDouble(chargeAmount.getText());
            } else if (nC.isSelected()) {
                microcoulombs = Double.parseDouble(chargeAmount.getText()) / 1000;
            } else {
                microcoulombs = Double.parseDouble(chargeAmount.getText()) / 1_000_000;
            }

            if (add.getText().equals("DODAJ")) {
                if (charges.get(identifier) != null) {
                    chargeExistsMessage(identifier);
                    return;
                }
                Vector2D randomVector = randomVector();
                while (charges.containsChargeAt(randomVector)) {
                    randomVector = randomVector();
                }
                charges.add(
                        new Charge(identifier, microcoulombs / 1_000_000, randomVector)
                );
            } else if (toRefactor != null) {
                Charge current = charges.get(identifier);
                if (current != null && toRefactor.getName().equals(identifier) || current == null) {
                    String oldName = toRefactor.getName();
                    toRefactor.setName(identifier);
                    charges.onChargeRenamed(oldName, identifier);
                    toRefactor.setChargeAmount(microcoulombs / 1_000_000);
                } else {
                    chargeExistsMessage(identifier);
                }
            }
            charges.recalculateForces();
            if (selectedCharge != null) {
                absoluteForce.setText(
                        selectedCharge + "\n|F| = " + formatter.format(selectedCharge.getTotalForce().norm() / 1E2) + " mN"
                );
            }
            repaint = true;
        } catch (Exception e) {
            if (e instanceof NumberFormatException) {
                invalidInput(chargeAmount.getText());
            }
        }

        if (repaint) {
            repaintBackground();
            repaintCharges();
            repaintForces();
        }

    }

    @FXML
    public void explanation() {

    }

    @FXML
    public void changeSelectedCharge() {
        add.setText(add.getText().equals("DODAJ") ? "PROMIJENI" : "DODAJ");
    }

    @FXML
    public void exit() {
        Platform.exit();
    }

    private void repaintBackground() {
        Painter.drawEmptyBackground(canvas);
    }

    private void repaintAll() {
        repaintBackground();
        repaintForces();
        repaintResultForce();
        repaintCharges();
    }

    private void repaintCharges() {
        charges.forEachCharge(
                c -> Painter.drawCharge(canvas, c, 16)
        );
    }

    private void repaintForces() {
        if (showVectors.isSelected()) {
            charges.forEachCharge(c ->
                c.forEachForce(force ->
                    Painter.paintVector(canvas, c, force, Info.vectorScaleConstant, false)
                )
            );
        }
    }

    private void repaintResultForce() {
        if (showResultVector.isSelected()) {
            charges.forEachCharge(c -> {
                Painter.paintVector(canvas, c, c.getTotalForce(), Info.vectorScaleConstant, true);
            });
        }
    }

    private Vector2D randomVector() {
        return RandomUtils.randomVector(
                70, canvas.getWidth(), 70, canvas.getHeight()
        );
    }

    private void invalidInput(String input) {
        showSimpleDialog("'" + input + "' nije ispravni broj.");
    }

    private void chargeExistsMessage(String name) {
        showSimpleDialog("Naboj sa imenom '" + name + "' već postoji.");
    }

    private void showSimpleDialog(String message) {
        try {
            Stage userInputStage = new Stage();
            VBox box = FXMLLoader.load(
                    Launcher.class.getResource(
                            "/elektrotehnika/simple_dialog.fxml"
                    )
            );
            ((Label) (box.getChildren().get(0))).setText(message);
            box.getChildren().get(1).setOnMouseClicked(event -> userInputStage.close());
            userInputStage.setScene(new Scene(box));
            userInputStage.show();
        } catch (IOException e) {

        }
    }

    private void setChargeTypes() {
        charges.forEachCharge(c -> c.setType(ChargeType.NORMAL));
        if (toHighlight != null) {
            toHighlight.setType(ChargeType.HIGHLIGHTED);
        }
        if (toRefactor != null) {
            toRefactor.setType(ChargeType.TO_REFACTOR);
        }
    }

    private void selectScaleConstant() {
        if (t1.isSelected()) {
            vectorScaleConstant = 1;
        } else if (t10.isSelected()) {
            vectorScaleConstant = 10;
        } else {
            vectorScaleConstant = 100;
        }
        charges.recalculateForces();
        repaintAll();
    }

    @FXML
    public void multiplicationConstant() {
        charges.recalculateForces();
        selectScaleConstant();
        if (selectedCharge != null) {
            absoluteForce.setText(
                    selectedCharge + "\n|F| = " + formatter.format(selectedCharge.getTotalForce().norm() / 1E2) + " mN"
            );
        }
        repaintAll();
    }

}
package elektrotehnika;

import custom.Vector2D;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import static java.lang.Math.abs;

public class Charge {

    private static DecimalFormat formatter = new DecimalFormat("0.00##");
    private String name;
    private double chargeAmount;
    private Vector2D position;
    private Map<String, Vector2D> forces = new HashMap<>();
    private Vector2D totalForce = new Vector2D(0, 0);
    private ChargeType type = ChargeType.NORMAL;

    public Charge(String name, double chargeAmount, Vector2D position) {
        this.name = name;
        this.chargeAmount = chargeAmount;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Vector2D getPosition() {
        return position;
    }

    public void setCenterX(double centerX) {
        position.setX(centerX);
    }

    public void setCenterY(double centerY) {
        position.setY(centerY);
    }

    public Map<String, Vector2D> getForces() {
        return forces;
    }

    public void force(double k, Charge other, Vector2D willBeRefactored) {
        double x = other.position.getX() - position.getX();
        double y = other.position.getY() - position.getY();

        switch (Info.distanceMeasure) {
            case 'c':
                x *= 1E-2;
                y *= 1E-2;
                break;
            case 'm':
                x *= 1E-3;
                y *= 1E-3;
                break;
            case 'u':
                x *= 1E-6;
                y *= 1E-6;
                break;
        }

        double distance = Math.sqrt(x * x + y * y);
        double amplitude = k * chargeAmount * other.chargeAmount / (distance * distance);

        x /= distance;
        y /= distance;

        x *= amplitude;
        y *= amplitude;

        x *= 100_000;
        y *= 100_000;

        willBeRefactored.setX(x);
        willBeRefactored.setY(y);
    }

    public void registerForceFrom(Charge another) {
        if (!forces.containsKey(another.name)) {
            forces.put(another.name, new Vector2D(0, 0));
        }
        force(-9 * 1E9, another, forces.get(another.name));
    }

    public void unregisterForceFrom(String otherName) {
        forces.remove(otherName);
        calculateTotalForce();
    }

    public void forEachForce(Consumer<Vector2D> action) {
        forces.values().forEach(action);
    }

    public void calculateTotalForce() {
        totalForce.setX(0);
        totalForce.setY(0);

        forces.values().forEach(force -> {
            totalForce.setX(totalForce.getX() + force.getX());
            totalForce.setY(totalForce.getY() + force.getY());
        });
    }

    public Vector2D getTotalForce() {
        return totalForce;
    }

    public ChargeType getType() {
        return type;
    }

    public void setType(ChargeType type) {
        this.type = type;
    }

    public String convertedChargeAmount(String unit) {
        formatter.format(chargeAmount);
        if (abs(chargeAmount) <= 1E-12) {
            return "|x| < 1 p" + unit;
        } else if (abs(chargeAmount) <= 1E-9) {
            return formatter.format(chargeAmount * 1E12) + " p" + unit;
        } else if (abs(chargeAmount) <= 1E-6) {
            return formatter.format(chargeAmount * 1E9) + " n" + unit;
        } else if (abs(chargeAmount) <= 1E-3) {
            return formatter.format(chargeAmount * 1E6) + " u" + unit;
        } else if (abs(chargeAmount) <= 1) {
            return formatter.format(chargeAmount * 1000) + " m" + unit;
        } else if (abs(chargeAmount) <= 1000) {
            return formatter.format(chargeAmount) + " " + unit;
        } else if (abs(chargeAmount) <= 1E6) {
            return formatter.format(chargeAmount / 1000) + " k" + unit;
        } else if (abs(chargeAmount) <= 1E9) {
            return formatter.format(chargeAmount / 1E6) + " M" + unit;
        } else {
            return "|x| > 1 G" + unit;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Charge charge = (Charge) o;
        return Objects.equals(name, charge.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }

}

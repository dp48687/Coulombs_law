package pointregister;

import custom.CustomPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static pointregister.PointRegister.LineType.NORMAL;
import static utils.CustomMath.pointLineDistance;
import static utils.CustomMath.twoPointsDistance;

public class PointRegister {

    private List<CustomPoint> points = new ArrayList<>();

    private List<Line> lines = new ArrayList<>();

    public static class Line implements Comparable<Line> {

        private CustomPoint from;

        private CustomPoint to;

        private double weight;

        private String name;

        private LineType lineType = NORMAL;

        public Line(CustomPoint index1, CustomPoint index2) {
            this.from = index1;
            this.to = index2;
        }

        public CustomPoint getFrom() {
            return from;
        }

        public CustomPoint getTo() {
            return to;
        }

        public double getWeight() {
            return weight;
        }

        public LineType getLineType() {
            return lineType;
        }

        @Override
        public int compareTo(Line o) {
            if (o.getWeight() < weight) {
                return 1;
            } else if (o.getWeight() > weight) {
                return -1;
            } else {
                return 0;
            }
        }

        @Override
        public boolean equals(Object obj) {
            try {
                Line other = (Line) obj;
                CustomPoint p1 = other.from;
                CustomPoint p2 = other.to;

                if (from.equals(p1) && to.equals(p2)) {
                    return true;
                } else if (from.equals(p2) && to.equals(p1)) {
                    return true;
                } else {
                    return false;
                }

            } catch (Exception e) {
                return false;
            }
        }

        @Override
        public String toString() {
            return from + " " + to + " -> " + weight;
        }

    }

    public enum LineType {
        NORMAL,
        HIGHLIGHTED
    }

    @Override
    public String toString() {
        if (points.isEmpty()) {
            return "";
        } else {
            StringBuilder pointRegString = new StringBuilder("");

            for (int i = 1; i <= points.size(); ++i) {
                pointRegString.append(i);
                pointRegString.append(" <- ");
                pointRegString.append(points.get(i - 1));
                pointRegString.append("\n");
            }

            pointRegString.append("#\n");


            lines.forEach(line ->
                    pointRegString.append(
                        "[" + (points.indexOf(line.from) + 1) + ", "
                            + (points.indexOf(line.to) + 1) + "] = "
                            + line.weight + "\n"
                    )
            );

            return pointRegString
                    .toString()
                    .substring(
                            0, pointRegString.toString().length() - 1
                    );
        }
    }

}

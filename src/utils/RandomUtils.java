package utils;

import custom.Vector2D;

import java.text.DecimalFormat;
import java.util.Random;

public class RandomUtils {
    private static Random random = new Random();

    public static Vector2D randomVector(double minX, double maxX, double minY, double maxY) {
        return new Vector2D(
                minX + (maxX - minX) * random.nextFloat(),
                minY + (maxY - minY) * random.nextFloat()
        );
    }

}

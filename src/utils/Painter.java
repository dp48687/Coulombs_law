package utils;

import custom.CustomPoint;
import custom.Vector2D;
import elektrotehnika.Charge;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import pointregister.PointRegister;

import java.util.Arrays;
import java.util.Objects;

public class Painter {

    private static final Color POINT_COLOR_NORMAL = Color.RED;
    private static final Color POINT_COLOR_NEIGHBOUR = Color.YELLOW;
    private static final Color POINT_COLOR_VISITED = Color.DARKGRAY;
    private static final Color POINT_RING_COLOR = Color.GRAY;
    private static final Color LINE_COLOR = Color.valueOf("#84b3ff");
    private static final Color INDEX_COLOR = Color.BLACK;
    private static final Color WEIGHT_COLOR = Color.RED;

    private static final double POINT_RADIUS_SMALL = 5;
    private static final double POINT_RADIUS_BIG = 45;

    private static final Color CHARGE_NORMAL_COLOR = Color.BLACK;
    private static final Color CHARGE_HIGHLIGHTED_COLOR = Color.GRAY;
    private static final Color CHARGE_TO_REFACTOR_COLOR = Color.valueOf("#cc630e");
    private static final Color CHARGE_TEXT_COLOR = Color.valueOf("#ef0202");

    private static double xPointsTemp[] = new double[3];
    private static double yPointsTemp[] = new double[3];


    public static void drawEmptyBackground(Canvas canvas) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(Paint.valueOf("#BBBBBB"));
        context.fillRect(
                0, 0, canvas.getWidth(), canvas.getHeight()
        );
        context.setFill(Paint.valueOf("#FFFFFF"));
        context.fillRect(
                1.5, 1.5, canvas.getWidth() - 3, canvas.getHeight() - 3
        );
        context.setFill(Paint.valueOf("#BBBBBB"));
    }

    public static void drawPoint(Canvas canvas, CustomPoint point, Object name) {
        GraphicsContext context = canvas.getGraphicsContext2D();

        Paint presentStroke = context.getStroke();
        switch (point.getType()) {
            case NORMAL:
                context.setStroke(POINT_COLOR_NORMAL);
                break;
            case NEIGHBOUR:
                context.setStroke(POINT_COLOR_NEIGHBOUR);
                break;
            case VISITED:
                context.setStroke(POINT_COLOR_VISITED);
        }
        context.strokeOval(point.getX() - POINT_RADIUS_SMALL / 2, point.getY() - POINT_RADIUS_SMALL / 2, POINT_RADIUS_SMALL, POINT_RADIUS_SMALL);
        context.setFill(INDEX_COLOR);
        context.fillText(
                name.toString(),
                point.getX() - 1.5,
                point.getY() - 4.5
        );
        context.setStroke(presentStroke);
    }

    public static void drawMarkedPoint(Canvas canvas, CustomPoint point, Object index) {
        drawPoint(canvas, point, index);

        GraphicsContext context = canvas.getGraphicsContext2D();
        Paint presentStroke = context.getStroke();
        context.setStroke(POINT_RING_COLOR);
        context.strokeOval(point.getX() - 5.5, point.getY() - 5.5, 11, 11);
        context.setStroke(presentStroke);
    }

    public static void drawLine(Canvas canvas, PointRegister.Line line, boolean isDirected) {
        Objects.requireNonNull(line);

        GraphicsContext context = canvas.getGraphicsContext2D();

        Paint strokeBefore = context.getStroke();
        double lineWidthBefore = context.getLineWidth();

        if (line.getLineType().equals(PointRegister.LineType.HIGHLIGHTED)) {
            context.setLineWidth(4);
        }

        context.setStroke(LINE_COLOR);
        context.strokeLine(
                line.getFrom().getX(), line.getFrom().getY(),
                line.getTo().getX(), line.getTo().getY()
        );

        if (isDirected) {

        }

        context.setStroke(strokeBefore);
        context.setLineWidth(lineWidthBefore);
    }

    public static void drawHighlightedLine(Canvas canvas, PointRegister.Line line, double width) {
        GraphicsContext context = canvas.getGraphicsContext2D();

        double lineWidthBefore = context.getLineWidth();
        context.setLineWidth(width);
        drawLine(canvas, line, false);

        context.setLineWidth(lineWidthBefore);
    }

    public static void drawText(Canvas canvas, double xPosition, double yPosition, Object text) {
        GraphicsContext context = canvas.getGraphicsContext2D();

        Paint presentFill = context.getFill();
        context.setFill(WEIGHT_COLOR);
        context.fillText(
                text.toString(),
                xPosition,
                yPosition
        );
        context.setFill(presentFill);

    }

    public static void drawCharge(Canvas canvas, Charge c, double radius) {
        Vector2D center = c.getPosition();

        GraphicsContext context = canvas.getGraphicsContext2D();

        Paint fillBefore = context.getFill();

        Color ringColor;
        switch (c.getType()) {
            case NORMAL:
                ringColor = CHARGE_NORMAL_COLOR;
                break;
            case TO_REFACTOR:
                ringColor = CHARGE_TO_REFACTOR_COLOR;
                break;
            default:
                ringColor = CHARGE_HIGHLIGHTED_COLOR;
                break;
        }

        context.setFill(ringColor);
        context.fillOval(
                center.getX() - radius / 2, center.getY() - radius / 2, radius, radius
        );
        context.setFill(Paint.valueOf("#FFFFFF"));
        context.fillOval(
                center.getX() - radius / 2 + 2, center.getY() - radius / 2 + 2, radius - 4, radius - 4
        );
        context.setFill(
                c.getChargeAmount() > 0 ? Paint.valueOf("#0000DD") : Paint.valueOf("#DD0000")
        );
        if (c.getChargeAmount() > 0) {
            context.fillRect(center.getX() - 1, center.getY() - radius / 2 + 4, 2, radius - 8);
        }
        context.fillRect(center.getX() - radius / 2 + 4, center.getY() - 1, radius - 8, 2);

        context.setFill(CHARGE_TEXT_COLOR);
        context.fillText(c.getName(), center.getX() - radius / 2, center.getY() + radius + 3.5);

        context.setFill(fillBefore);
    }

    public static void paintVector(Canvas canvas, Charge charge, Vector2D vector, double scaleConstant, boolean highlight) {
        GraphicsContext context = canvas.getGraphicsContext2D();

        double xFrom = charge.getPosition().getX();
        double yFrom = charge.getPosition().getY();

        Paint strokeBefore = context.getStroke();
        Paint fillBefore = context.getFill();

        context.setFill(Color.BLACK);
        double lineWidth = context.getLineWidth();
        if (highlight) {
            context.setStroke(LINE_COLOR);
            context.setFill(LINE_COLOR);
            context.setLineWidth(4);
        }

        drawArrow(
                canvas,
                getAngle(vector.getX(), vector.getY()),
                charge.getPosition().getX() + vector.getX() * scaleConstant,
                charge.getPosition().getY() + vector.getY() * scaleConstant
        );

        context.strokeLine(
                xFrom,
                yFrom,
                xFrom + vector.getX() * scaleConstant,
                yFrom + vector.getY() * scaleConstant
        );

        context.setLineWidth(lineWidth);
        context.setStroke(strokeBefore);
        context.setFill(fillBefore);
    }

    public static void drawArrow(Canvas canvas, double angle, double translateX, double translateY) {
        GraphicsContext context = canvas.getGraphicsContext2D();

        double xHalf = 5;
        double y = 12;
        double cosa = Math.cos(angle - Math.PI / 2);
        double sina = Math.sin(angle - Math.PI / 2);

        xPointsTemp[0] = xHalf * cosa + translateX;
        yPointsTemp[0] = xHalf * sina + translateY;
        xPointsTemp[1] = -xHalf * cosa + translateX;
        yPointsTemp[1] = -xHalf * sina + translateY;
        xPointsTemp[2] = -y * sina + translateX;
        yPointsTemp[2] = y * cosa + translateY;

        context.fillPolygon(xPointsTemp, yPointsTemp, 3);

    }

    private static Double getAngle(double deltaX, double deltaY) {
        if (Double.compare(Math.abs(deltaX), 1E-6) <= 0) {
            return deltaY > 0 ? Math.PI / 2 : -Math.PI / 2;
        } else if (Double.compare(Math.abs(deltaY), 1E-6) <= 0) {
            return deltaX > 0 ? 0 : Math.PI;
        } else {
            return Math.atan2(deltaY, deltaX);
        }
    }

}

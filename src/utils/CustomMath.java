package utils;

import custom.CustomPoint;
import pointregister.PointRegister;

public class CustomMath {

    public static double pointLineDistance(CustomPoint point, PointRegister.Line line) {
        double fromX = line.getFrom().getX();
        double fromY = line.getFrom().getY();
        double toX = line.getTo().getX();
        double toY = line.getTo().getY();

        double k = (toY - fromY) / (toX - fromX);
        if (!Double.isFinite(k)) {
            k = 1E6;
        }


        double l1 = toY - k * toX;
        double l2 = point.getY() + k * point.getX();

        double yS = (l1 + l2) / 2;
        double xS = (l2 - l1) / (2 * k);
        if (!Double.isFinite(xS)) {
            xS = 1E6;
        }

        return Math.sqrt(
                        (yS - point.getY()) * (yS - point.getY()) +
                        (xS - point.getX()) * (xS - point.getX())
        );
    }

    public static double twoPointsDistance(CustomPoint point1, CustomPoint point2) {
        double x1 = point1.getX();
        double y1 = point1.getY();
        double x2 = point2.getX();
        double y2 = point2.getY();

        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

}

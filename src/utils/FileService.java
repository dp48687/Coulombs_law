package utils;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileService {

    public static File selectFile(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose .txt file");
        fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("Text files", ".txt")
        );
        return fileChooser.showOpenDialog(stage);
    }

    public static File selectFolder(Stage stage) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose folder");
        return new DirectoryChooser().showDialog(stage);
    }

    public static void saveToFile(File selectedFolder, String contentToWrite) {
        try {
            Path newFile = Paths.get(
                    selectedFolder.getAbsolutePath()
            ).resolve("koraci.txt");

            int counter = 0;
            while (Files.exists(newFile)) {
                newFile = Paths.get(
                        selectedFolder.getAbsolutePath()
                ).resolve("koraci (" + ++counter + ").txt");
            }

            Files.createFile(newFile);

            OutputStream os = new FileOutputStream(newFile.toString());

            byte[] bytesToWrite = contentToWrite.getBytes();
            int bufferSize = 1024;
            int writtenSoFar = 0;

            while (writtenSoFar < bytesToWrite.length) {
                os.write(
                        bytesToWrite,
                        writtenSoFar,
                        writtenSoFar + bufferSize < bytesToWrite.length ?
                                bufferSize :
                                bytesToWrite.length - writtenSoFar
                );
                writtenSoFar += bufferSize;
            }
        } catch (Exception e) {

        }
    }

}
